# Quasar App (realimob)

Version 1

## Install the dependencies
```bash
npm install
```

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```

### Lint the files
```bash
npm run lint
```

### Build the app for production
```bash
quasar build
```

### Added components
- VueParticles
```bash
npm install vue-particles
```

### Templates used
- Login
```bash
https://github.com/WebDevChallenges/vue-login
```
### Customize the configuration
See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).
