import axios from 'axios'
import { LocalStorage } from 'quasar' // make sure you edit quasar.conf.js

const apiCall = {

  typeCall: 'get',

  setUpAxios () {
    axios.defaults.baseURL = 'http://localhost:3030'
    axios.defaults.headers.common['Authorization'] = LocalStorage.getItem('feathers-jwt')
    axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'
    axios.defaults.headers.post['Cache-Control'] = 'no-cache, no-store, must-revalidate'
    // axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'
    axios.defaults.headers.get['Content-Type'] = 'application/x-www-form-urlencoded'
    axios.defaults.headers.get['Cache-Control'] = 'no-cache, no-store, must-revalidate' // HTTP 1.1.
    // axios.defaults.headers.get['Cache-Control'] = 'no-cache, no-store, must-revalidate' // HTTP 1.1.
    // response.setHeader("Pragma", "no-cache"); // HTTP 1.0.
    // response.setHeader("Expires", "0"); // Proxies.
  },

  getCallApi (link, method, extraParams, configAxiosArg, returnPromiseImage, postReturnImage) {
    let options = {}
    returnPromiseImage = returnPromiseImage || false
    postReturnImage = postReturnImage || false
    extraParams = extraParams || false
    configAxiosArg = configAxiosArg || {}
    configAxiosArg.crossdomain = true
    this.typeCall = 'get'

    if (extraParams) {
      this.typeCall = method
      options.method = this.typeCall
      options.data = extraParams
    }
    if (method === 'delete' || method === 'DELETE') {
      this.typeCall = method
    }
    options.config = configAxiosArg

    try {
      if (returnPromiseImage) {
        return axios[this.typeCall](link, configAxiosArg).then((response) => {
          const reader = new window.FileReader()
          reader.readAsDataURL(response.data)
          return reader
        })
      } else {
        return axios[this.typeCall](link, extraParams, configAxiosArg).then(result => {
          if (postReturnImage) {
            // console.log(result.data)
            const reader = new window.FileReader()
            reader.readAsDataURL(result.data)
            return reader
          } else {
            console.log('axios result: ', result)
            return result
          }
        })
      }
    } catch (error) {
      console.log('axios errorJson: ', error)
      return error
    }
  }
}

export default apiCall
