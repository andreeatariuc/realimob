import feathers from '@feathersjs/feathers'
import socketio from '@feathersjs/socketio-client'
import auth from '@feathersjs/authentication-client'
import io from 'socket.io-client'

const socket = io('http://localhost:3030', { transports: ['websocket'] })

const api = feathers()
  .configure(socketio(socket))
  .configure(auth({ storage: window.localStorage }))
  .hooks()

// api.service('/users')
api.service('authmanagement')
api.service('menu')

socket.on('authenticated', data => {
  console.log('socketio - channel:authenticated => ', data)
})

socket.on('connections', data => {
  console.log('socketio - channel:connections => ', data)
})
export default api
