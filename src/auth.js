// Import the Feathers client module that we've created before
import api from 'src/api'

const auth = {

  // keep track of the logged in user
  user: null,

  getUser () {
    return this.user
  },

  async fetchUser (accessToken) {
    return api.authentication.setAccessToken(accessToken)
      .then(async () => {
        let payload = await api.get('authentication')
        // console.log(payload)
        return payload.user// api.service('users').get(payload.user.id)
      })
      .then(user => {
        return Promise.resolve(user)
      })
  },

  authenticate () {
    return api.authenticate()
      .then(async (response) => {
        let payload = await this.fetchUser(response.accessToken)
        return payload
      })
      .then(user => {
        this.user = user
        return Promise.resolve(user)
      })
      .catch((err) => {
        this.user = null
        return Promise.reject(err)
      })
  },

  authenticated () {
    return this.user !== null
  },

  signout () {
    return api.logout()
      .then(() => {
        this.user = null
      })
      .catch((err) => {
        return Promise.reject(err)
      })
  },

  onLogout (callback) {
    api.on('logout', () => {
      this.user = null
      callback()
    })
  },

  onAuthenticated (callback) {
    api.on('authenticated', response => {
      this.fetchUser(response.accessToken)
        .then(user => {
          this.user = user
          callback(this.user)
        })
        // eslint-disable-next-line handle-callback-err
        .catch((err) => {
          callback(this.user)
        })
    })
  },

  async register (userData) {
    if (!userData.status) {
      console.info('default userData status insered: 1')
      userData.status = 1
    }
    if (!userData.terms_accepted) {
      console.info('default userData terms_accepted insered: 1')
      userData.terms_accepted = 1
    }
    if (!userData.concent_accepted) {
      console.info('default userData concent_accepted insered: 1')
      userData.concent_accepted = 1
    }
    if (!userData.createdBy) {
      console.info('default userData concent_accepted insered: 0')
      userData.createdBy = 0
    }

    console.log(userData)
    return api.service('users').create(userData)
  },

  async reAuth () {
    try {
      return await api.reAuthenticate()
        .then(async (response) => {
          await this.fetchUser(response.accessToken)
          return response
        })
        .then(user => {
          this.user = user
          return Promise.resolve(user)
        })
        .catch((err) => {
          this.user = null
          return Promise.reject(err)
        })
      // return await api.reAuthenticate()
    } catch (e) {
      console.log('reAuth ERROR:', e)
      return false
    }
  },

  async login (email, password) {
    // try {
    //   // First try to log in with an existing JWT
    //   return await api.reAuthenticate()
    // } catch (error) {
    //   // If that errors, log in with email/password
    //   // Here we would normally show a login page
    //   // to get the login information
    //   // console.log('auth file error then, show login page... error:', error)
    // }
    return api.authenticate({
      strategy: 'local',
      email: email,
      password: password
    }).catch((e) => {
      console.log(e)
    })

    // return api.authenticate({
    //   strategy: 'local',
    //   email: email,
    //   password: password
    // })
  }

}

export default auth
