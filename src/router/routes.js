// <<<<<<< HEAD
// import login from '../pages/auth/login'
// import signup from '../pages/auth/signup'
// import main from '../layouts/main'
// import recoverPassword from '../pages/auth/recoverPassword'
// import newPassword from '../pages/auth/newPassword'
// import home from '../pages/main/home'
// import dashboard from '../pages/main/dashboard'
// import rc from '../pages/main/rc'
// import general from '../pages/main/general'
//
// const routes = [
//   {
//     path: '/',
//     component: login,
//     name: 'login',
//     children: [
//

const routes = [
  { path: '/', component: () => import('../pages/auth/login'), name: 'login', children: [], meta: { requiresAuth: false } },
  { path: '/signup', name: 'signup', component: () => import('../pages/auth/signup'), meta: { requiresAuth: false } },
  { path: '/verify', name: 'verify', component: () => import('../pages/auth/verify'), meta: { requiresAuth: false } },
  { path: '/recoverPassword', name: 'recoverPassword', component: () => import('../pages/auth/recoverPassword'), meta: { requiresAuth: false } },
  { path: '/newPassword', name: 'newPassword', component: () => import('../pages/auth/newPassword'), meta: { requiresAuth: false } },
  { path: '/main',
    component: () => import('layouts/main'),
    name: 'main',
    children: [
      {
        path: '/',
        name: 'home',
        component: () => import('../pages/main/home'),
        meta: { requiresAuth: true }
      },
      {
        path: '/dashboard',
        name: 'dashboard',
        component: () => import('../pages/main/dashboard'),
        meta: { requiresAuth: true }
      },
      {
        path: '/user',
        name: 'userPage',
        component: () => import('../pages/main/userPage')
      },
      {
        path: '/rc',
        name: 'rc',
        component: () => import('../pages/main/rc'),
        meta: { requiresAuth: true }
      },
      {
        path: '/rc/rc_residential_complexes',
        name: 'rc_residential_complexes',
        component: () => import('../pages/main/rc/rc_residential_complexes'),
        meta: { requiresAuth: true }
      },
      {
        path: '/rc/rc_residential_complexes/residentialComplexEditViewAdd/:id',
        name: 'residentialComplexEditViewAdd',
        component: () => import('../components/tables/edit/rc/residentialComplexEditViewAdd'),
        meta: { requiresAuth: true }
      },
      {
        path: '/rc/rc_buildings',
        name: 'rc_buildings',
        component: () => import('../pages/main/rc/rc_buildings'),
        meta: { requiresAuth: true }
      },
      {
        name: 'buildingEditViewAdd',
        path: '/rc/rc_buildings/buildingEditViewAdd/:id',
        component: () => import('../components/tables/edit/rc/buildingEditViewAdd'),
        meta: { requiresAuth: true }
      },
      {
        path: '/rc/rc_flats',
        name: 'rc_flats',
        component: () => import('../pages/main/rc/rc_flats'),
        meta: { requiresAuth: true }
      },
      {
        name: 'flatEditViewAdd',
        path: '/rc/rc_flats/flatEditViewAdd/:id',
        component: () => import('../components/tables/edit/rc/flatEditViewAdd'),
        meta: { requiresAuth: true }
      },
      {
        path: '/rc/rc_flats_types',
        name: 'rc_flats_types',
        component: () => import('../pages/main/rc/rc_flats_types'),
        meta: { requiresAuth: true }
      },
      // {
      //   name: 'flatTypesEditViewAdd',
      //   path: '/rc/rc_flats_types/flatTypesEditViewAdd/:id',
      //   component: () => import('../components/tables/edit/rc/flatTypesEditViewAdd'),
      //   meta: { requiresAuth: true }
      // },
      {
        path: '/general',
        name: 'general',
        component: () => import('../pages/main/general'),
        meta: { requiresAuth: true }
      },
      {
        path: '/settings',
        name: 'settings',
        component: () => import('../pages/main/settings'),
        meta: { requiresAuth: true }
      },
      {
        name: 's_users',
        path: '/settings/s_users',
        component: () => import('../pages/main/settings/s_users'),
        meta: { requiresAuth: true }
      },
      {
        name: 'userEditViewAdd',
        path: '/settings/s_users/userEditViewAdd/:id',
        component: () => import('../components/tables/edit/settings/userEditViewAdd'),
        meta: { requiresAuth: true }
      },
      {
        name: 's_permissions',
        path: '/settings/s_permissions',
        component: () => import('../pages/main/settings/s_permissions'),
        meta: { requiresAuth: true }
      },
      {
        name: 's_roles',
        path: '/settings/s_roles',
        component: () => import('../pages/main/settings/s_roles'),
        meta: { requiresAuth: true }
      },
      {
        name: 's_statuses',
        path: '/settings/s_statuses',
        component: () => import('../pages/main/settings/s_statuses'),
        meta: { requiresAuth: true }
      },
      {
        name: 's_settlements',
        path: '/settings/s_settlements',
        component: () => import('../pages/main/settings/s_settlements'),
        meta: { requiresAuth: true }
      },
      {
        name: 's_documents_types',
        path: '/settings/s_documents_types',
        component: () => import('../pages/main/settings/s_documents_types'),
        meta: { requiresAuth: true }
      }
    ],
    meta: { requiresAuth: true }
  },

  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
