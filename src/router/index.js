import Vue from 'vue'
import VueRouter from 'vue-router'

import routes from './routes'
import auth from '../auth'
// import { SessionStorage } from 'quasar'

Vue.use(VueRouter)

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation
 */

export default function (/* { store, ssrContext } */) {
  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes,

    // Leave these as is and change from quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  })

  Router.beforeEach(async (to, from, next) => {
    let isAuth = await auth.authenticated()
    if (!isAuth) {
      isAuth = await auth.reAuth()
    }
    // if (!SessionStorage.has('sessionUser') && isAuth) {
    //   console.log(isAuth)
    //   // Exists session session restore AND SET SESSION
    //   SessionStorage.set('sessionUser', { userId: isAuth.user.name, timesLoggedIn: 0, inactivityReport: [] })
    // }
    // console.log('I AM SESSION: ', SessionStorage.getAll())

    if (!to.meta.requiresAuth || isAuth) {
      // All is okay, let the route change continue
      if (isAuth && (to.name === 'login' || to.name === null)) {
        next({ path: '/main' })
      } else {
        next()
      }
    } else {
      // console.log('Not authenticated', to, from)
      // Cancel the route change and redirect back to the Home page
      next({ name: 'login' })
    }
  })

  return Router
}
