import apiCall from '../../apiCall'

export function dataActualViewEditAddComplex ({ dispatch, commit }, payload) {
  if (payload) {
    apiCall.getCallApi('/residential_complexes/' + payload, 'post').then(response => {
      if (response.data) {
        let complex = response.data
        commit('setActualViewEditAddComplex', complex)
      } else {
        console.error('The complex key provided is wrong or does not exist!')
      }
    })
  } else {
    console.error('No complex key to search for!')
  }
}

export function saveComplex ({ commit }, payload) {
  let createXORsave = payload.id ? '/' + payload.id : ''
  let method = payload.id ? 'patch' : 'post'
  return apiCall.getCallApi('/residential_complexes' + createXORsave, method, payload.data).then(response => {
    commit('complexes/saveComplex', response.data, { root: true })
    commit('setActualViewEditAddComplex', response.data)
    return response.data.id
  })
}
