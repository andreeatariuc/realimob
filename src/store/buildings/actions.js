import apiCall from '../../apiCall'

export function dataBuildings ({ state, commit }) {
  return apiCall.getCallApi('/buildings', 'post').then(response => {
    let temp = []
    if (response && response.data) {
      for (let item in response.data) {
        temp.push({
          number: (parseInt(item) + 1),
          building_id: response.data[item].id,
          id: response.data[item].id,
          name: response.data[item].name,
          residential_complex_id: response.data[item].residential_complex_id,
          description: response.data[item].description,
          no_floors: response.data[item].no_floors,
          no_apartments: response.data[item].no_apartments,
          indoor_parking_places: response.data[item].indoor_parking_places,
          outdoor_parking_places: response.data[item].outdoor_parking_places
        })
      }
    }
    commit('getDataBuildings', temp)
    return true
  })
}

// export function saveBuilding ({ commit }, payload) {
//   let createXORsave = payload.id ? '/' + payload.id : ''
//   let method = payload.id ? 'patch' : 'post'
//
//   apiCall.getCallApi('/buildings' + createXORsave, method, payload.data).then(response => {
//     commit('saveBuilding', response.data)
//   })
// }
