export function getDataBuildings (state, payload) {
  state.buildings = payload
}

export function saveBuilding (state, payload) {
  let building = state.buildings.findIndex(x => x.id === payload.id)

  if (building) {
    let buildingToChange = state.buildings[building]
    for (let keyBuilding in buildingToChange) {
      buildingToChange[keyBuilding] = payload[keyBuilding]
    }
    state.buildings[building] = buildingToChange
  } else {
    let number = state.buildings.length
    let newBuilding = {
      number: number,
      building_id: payload.id,
      id: payload.id,
      name: payload.name,
      description: payload.description,
      no_floors: payload.no_floors,
      no_apartments: payload.no_apartments,
      indoor_parking_places: payload.indoor_parking_places,
      outdoor_parking_places: payload.outdoor_parking_places
    }
    state.buildings.push(newBuilding)
  }
}
