export function getBuilding (state) {
  return state.building
}

export function getActualViewEditAddBuilding (state) {
  return state.actualViewEditAddBuilding
}

export function buildingStatuses (state) {
  return state.statuses
}
