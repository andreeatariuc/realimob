import apiCall from '../../apiCall'

export function dataBuilding ({ state, commit }) {
  return apiCall.getCallApi('/building', 'post').then(response => {
    let temp = []
    if (response && response.data) {
      for (let item in response.data.data) {
        temp.push({
          number: (parseInt(item) + 1),
          building_id: response.data.data[item].id,
          id: response.data.data[item].id,
          name: response.data.data[item].name,
          description: response.data.data[item].description,
          no_floors: response.data.data[item].no_floors,
          no_apartments: response.data.data[item].no_apartments,
          indoor_parking_places: response.data.data[item].indoor_parking_places,
          outdoor_parking_places: response.data.data[item].outdoor_parking_places,
          phone: response.data.data[item].phone
        })
      }
    }
    commit('getDataBuilding', temp)
    return true
  })
}

export const dataActualViewEditAddBuilding = ({ dispatch, commit }, payload) => {
  if (payload) {
    apiCall.getCallApi('/buildings/' + payload, 'post').then(response => {
      if (response && response.data) {
        let building = response.data
        building.description = building.description !== null ? building.description : ''
        commit('setActualViewEditAddBuilding', building)
      } else {
        console.error('The building key provided is wrong or does not exist!')
      }
    })
  } else {
    console.error('No building key to search for!')
  }
}

export const getStatusesData = ({ dispatch, commit }) => {
  return apiCall.getCallApi('/buildings-statuses/', 'post').then(response => {
    if (response && response.data) {
      let statuses = response.data
      commit('setStatusesData', statuses)
      return true
    } else {
      console.error('Something went wrong! Try again!!!')
      return false
    }
  })
}

export function saveBuilding ({ commit }, payload) {
  let createXORsave = payload.id ? '/' + payload.id : ''
  let method = payload.id ? 'patch' : 'post'

  return apiCall.getCallApi('/buildings' + createXORsave, method, payload.data).then(response => {
    commit('setActualViewEditAddBuilding', response.data)
    return response.data.id
  })
}
