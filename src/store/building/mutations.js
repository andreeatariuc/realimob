export function getBuilding (state, payload) {
  state.building = payload
}

export const setActualViewEditAddBuilding = (state, payload) => {
  state.actualViewEditAddBuilding = payload
}

export const setStatusesData = (state, payload) => {
  state.statuses = payload
}
