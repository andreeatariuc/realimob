import apiCall from '../../apiCall'

export function dataDocumentsTypes ({ state, commit }) {
  return apiCall.getCallApi('/documents_types', 'post').then(response => {
    let temp = []
    if (response && response.data) {
      for (let item in response.data) {
        temp.push({
          number: (parseInt(item) + 1),
          documents_type_id: response.data[item].id,
          id: response.data[item].id,
          name: response.data[item].name,
          inactive: response.data[item].inactive
        })
      }
    }
    commit('getDataDocumentsTypes', temp)
    return true
  })
}
