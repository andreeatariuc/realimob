import apiCall from '../../apiCall'

export function dataUsers ({ state, commit }) {
  return apiCall.getCallApi('/users', 'post').then(response => {
    let temp = []
    if (response && response.data) {
      for (let item in response.data.data) {
        temp.push({
          number: (parseInt(item) + 1),
          uid: response.data.data[item].id,
          id: response.data.data[item].id,
          email: response.data.data[item].email,
          name: response.data.data[item].name,
          phone: response.data.data[item].phone
        })
      }
    }
    commit('getDataUsers', temp)
    return true
  })
}
