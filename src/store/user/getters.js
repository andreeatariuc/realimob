export function getUser (state) {
  return state.user
}

export function getPermissions (state) {
  return state.permissions
}

export function getUserRole (state) {
  return state.role
}

export function getActualViewEditAddUser (state) {
  return state.actualViewEditAddUser
}
