import { LocalStorage } from 'quasar' // make sure you edit quasar.conf.js
/* *
 * Only for the first time you want to set a user
 * */
export const userSet = (state, payload) => {
  // console.log((state.user && !state.user.id), state, !state.user.id)
  if (state.user && !state.user.id) {
    // console.log('muttation - userSet', payload)
    state.user = payload
    state.token = LocalStorage.getItem('feathers-jwt')
  } else {
    console.error('You can\'t use set user mutation, if is already a user set! Please check your code and use update for this mutation of user!')
  }
}

/* *
 * Only for the first time you want to set a user
 * */
export const userUpdate = (state, payload) => {
  state.user = payload
  state.token = LocalStorage.getItem('feathers-jwt') || false
}

/* *
 * Only for the first time you want to set a role
 * */
export const roleUpdate = (state, payload) => {
  state.role = payload
  state.token = LocalStorage.getItem('feathers-jwt') || false
}

/* *
 * Only for the first time you want to set a roles for complexes
 * */
export const setRole = (state, payload) => {
  // console.log((state.user && !state.user.id), state, !state.user.id)
  if (state.role && state.role.length === 0) {
    // console.log('muttation - setRole', payload)
    state.role = payload
    // state.token = LocalStorage.getItem('feathers-jwt') || false
  } else {
    console.error('You can\'t use set role mutation, if is already a role set! Please check your code and use update for this mutation of role!')
  }
}

export const setActualViewEditAddUser = (state, payload) => {
  state.actualViewEditAddUser = payload
}
