import apiCall from '../../apiCall'

/*
 * Set user data for first time!
 * This function can't be used if there is already a user set
 */
// import { LocalStorage } from 'quasar'

export const userSet = ({ dispatch, commit, state }, payload) => {
  // console.log('userSet', payload)

  if (payload.userRoles) {
    let userRoles = payload.userRoles
    let menu = payload.userRoles.menu || {}

    delete payload.userRoles.menu
    delete userRoles.menu
    commit('setRole', userRoles)
    dispatch('config/menuSet', menu, { root: true })
    delete payload.userRoles

    // console.log(userRoles)
  }
  // console.log((state.user && !state.user.id), state.user, !state.user.id)
  if (state.user && !state.user.id) {
    // if (LocalStorage.getItem('feathers-jwt') && payload.id) {
    //   console.error('You have a problem with the token register! This is executed before token is created in LocalStorage!')
    // }
    commit('userSet', payload)
  } else {
    console.error('You can\'t use set user action, if is already a user set! Please check your code and use update for this action!')
  }
}

export const userUpdate = ({ dispatch, commit }, payload) => {
  payload = payload || false

  if (payload) {
    commit('userUpdate', payload)
  } else {
    console.error('You can\'t use "update" user action, if you want to unset user! Please check your code and use "userUnset" for this action!')
  }
}

export const userUnset = ({ dispatch, commit }, payload) => {
  commit('userUpdate', {})
}

export const dataActualViewEditAddUser = ({ dispatch, commit }, payload) => {
  let user = {}
  /* Start: Aici initializam toate variabilele folosite in caz ca nu avem id inseamna ca avem add */
  user.contact = {}
  /* End: Aici initializam toate variabilele folosite in caz ca nu avem id inseamna ca avem add */
  if (payload) {
    apiCall.getCallApi('/users/' + payload, 'post').then(response => {
      if (response && response.data) {
        user = response.data
        user.temporaryAccess = response.data.temporary_access !== null ? response.data.temporary_access : false
        user.tempAccessStartDate = response.data.tempAccessStartDate !== null ? response.data.tempAccessStartDate : ''
        // Definim capurile goale pentru contact ca sa nu le definim de 2 ori mai jos si apoi le suprascriem daca avem un contact in definit din baza de date
        if (user.contact_id) {
          apiCall.getCallApi('/contacts/' + user.contact_id, 'post').then(response => {
            if (response && response.data) {
              // Aici se baga raspunsul in user inainte de commit
              user.contact = response.data
              commit('setActualViewEditAddUser', user)
            } else {
              commit('setActualViewEditAddUser', user)
            }
          })
        } else {
          commit('setActualViewEditAddUser', user)
        }
      } else {
        console.error('The user key provided is wrong or does not exist!')
      }
    })
  } else {
    console.error('No user key to search for!')
  }
}
