import apiCall from '../../apiCall'

export function dataFlats ({ state, commit }) {
  return apiCall.getCallApi('/flats', 'post').then(response => {
    let temp = []
    if (response && response.data) {
      for (let item in response.data) {
        temp.push({
          number: (parseInt(item) + 1),
          id: response.data[item].id,
          name: response.data[item].name,
          no_rooms: response.data[item].no_rooms,
          floor: response.data[item].floor === 0 ? 'Parter' : response.data[item].floor,
          building_id: response.data[item].building_id
        })
      }
    }
    commit('getDataFlats', temp)
    return true
  })
}
