import apiCall from '../../apiCall'
export const dataActualViewEditAddFlat = ({ dispatch, commit }, payload) => {
  if (payload) {
    apiCall.getCallApi('/flats/' + payload, 'post').then(response => {
      if (response && response.data) {
        let flat = response.data
        flat.description = flat.description !== null ? flat.description : ''
        commit('setActualViewEditAddFlat', flat)
      } else {
        console.error('The flat key provided is wrong or does not exist!')
      }
    })
  } else {
    console.error('No flat key to search for!')
  }
}

export const getStatusesData = ({ dispatch, commit }) => {
  apiCall.getCallApi('/buildings-statuses/', 'post').then(response => {
    if (response && response.data) {
      let statuses = response.data
      commit('setStatusesData', statuses)
    } else {
      console.error('Something went wrong! Try again!!!')
    }
  })
}
