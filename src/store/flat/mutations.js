export const setActualViewEditAddFlat = (state, payload) => {
  state.actualViewEditAddFlat = payload
}

export const setStatusesData = (state, payload) => {
  state.statuses = payload
}
