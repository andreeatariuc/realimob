import apiCall from '../../apiCall'

export function complexesSet ({ state, commit }) {
  return apiCall.getCallApi('/residential_complexes', 'post').then(response => {
    let temp = []
    if (response && response.data) {
      for (let item in response.data) {
        temp.push({
          number: (parseInt(item) + 1),
          id: response.data[item].id,
          name: response.data[item].name,
          company_id: response.data[item].company_id,
          owner_contact_id: response.data[item].owner_contact_id,
          archived: response.data[item].archived,
          public: response.data[item].public,
          address_body: response.data[item].address_body,
          postal_code: response.data[item].postal_code,
          phone: response.data[item].phone,
          email: response.data[item].email,
          contact_id: response.data[item].contact_id,
          language: response.data[item].language,
          country: response.data[item].country,
          documents: response.data[item].documents
        })
      }
    }
    commit('getComplexesSet', temp)
    return true
  })
}

export function setComplex ({ dispatch, commit, state }, payload) {
  commit('setComplex', payload)
}
