export function getComplexesSet (state, payload) {
  state.complexes = payload
}

export function setComplex (state, payload) {
  state.complex = payload
}

export const saveComplex = (state, payload) => {
  let complex = state.complexes.findIndex(x => x.id === payload.id)

  if (complex !== -1) {
    state.complexes[complex] = payload
  } else {
    let number = state.complexes.length
    let newComplex = payload
    newComplex.number = number
    state.complexes.push(newComplex)
  }
}
