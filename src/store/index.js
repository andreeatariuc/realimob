import Vue from 'vue'
import Vuex from 'vuex'
import VueParticles from 'vue-particles'
// import createPersistedState from 'vuex-persistedstate'
// import example from './module-example'
import config from './config'
import user from './user'
import users from './users'
import complexes from './complexes'
import complex from './complex'
import documents from './documents'
import building from './building'
import buildings from './buildings'
import flats from './flats'
import flat from './flat'
import settlements from './settlements'
import roles from './roles'
import documentsTypes from './documents_types'
import flatsTypes from './flats_types'

Vue.use(Vuex)
Vue.use(VueParticles)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

export default function (/* { ssrContext } */) {
  return new Vuex.Store({
    modules: {
      user,
      users,
      config,
      complexes,
      complex,
      documents,
      building,
      buildings,
      flats,
      flat,
      settlements,
      roles,
      documentsTypes,
      flatsTypes
    },
    // plugins: [createPersistedState()],
    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEV
  })
  // return Store
}
