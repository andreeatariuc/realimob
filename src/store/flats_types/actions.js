import apiCall from '../../apiCall'

export function dataFlatsTypes ({ state, commit }) {
  return apiCall.getCallApi('/flats-types', 'post').then(response => {
    let temp = []
    if (response && response.data) {
      for (let item in response.data) {
        temp.push({
          number: (parseInt(item) + 1),
          flats_type_id: response.data[item].id,
          id: response.data[item].id,
          name: response.data[item].name,
          residential_complex_id: response.data[item].residential_complex_id,
          building_id: response.data[item].building_id,
          util_sqm: response.data[item].util_sqm,
          total_sqm: response.data[item].total_sqm,
          no_rooms: response.data[item].no_rooms,
          no_baths: response.data[item].no_baths,
          no_bedrooms: response.data[item].no_bedrooms,
          no_teraces: response.data[item].no_teraces,
          image_schema_thumb: response.data[item].image_schema_thumb,
          image_schema: response.data[item].image_schema,
          price_net: response.data[item].price_net,
          vat: response.data[item].vat,
          currency: response.data[item].currency
        })
      }
    }
    commit('getDataFlatsTypes', temp)
    return true
  })
}
