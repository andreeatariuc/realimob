import apiCall from '../../apiCall'

export function dataRoles ({ state, commit }) {
  return apiCall.getCallApi('/roles', 'post').then(response => {
    let temp = []
    console.log('action', response)
    if (response && response.data) {
      for (let item in response.data.data) {
        temp.push({
          number: (parseInt(item) + 1),
          role_id: response.data.data[item].id,
          id: response.data.data[item].id,
          name: response.data.data[item].name,
          description: response.data.data[item].description,
          is_superadmin: response.data.data[item].is_superadmin
        })
      }
    }
    commit('getDataRoles', temp)
    return true
  })
}
