import apiCall from '../../apiCall'
export function dataSettlements ({ commit }) {
  return apiCall.getCallApi('/settlements', 'post').then(response => {
    let settlementsOptions = []
    let settlements = []
    if (response && response.data) {
      for (let item in response.data) {
        settlements.push({
          number: (parseInt(item) + 1),
          settlement_id: response.data[item].id,
          id: response.data[item].id,
          name: response.data[item].name,
          type: response.data[item].type
        })
        if (parseInt(response.data[item].deletedBy) === 0) {
          settlementsOptions.push({ value: response.data[item].id, label: response.data[item].name })
        }
      }
      commit('getDataSettlementsOptions', settlementsOptions)
      commit('getDataSettlements', settlements)
      return true
    }
  })
}
