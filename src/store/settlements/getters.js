export function getDataSettlements (state) {
  return state.settlements
}

export function getDataSettlementsOptions (state) {
  return state.settlementsOptions
}
