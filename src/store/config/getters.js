export function leftDrawerOpen (state) {
  return state.leftDrawerOpen
}

export function showDrawerButton (state) {
  return state.showDrawerButton
}

export function showDrawerButtonMini (state) {
  return state.drawerButtonMini
}

export function showMiniModeForced (state) {
  return state.miniModeForced
}
export function activePath (state) {
  return state.activePath
}

export function userMenuData (state) {
  return state.menu
}
