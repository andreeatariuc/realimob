export function updateDrawerState (state, payload) {
  state.leftDrawerOpen = payload
}

export function updateDrawerButtonMini (state, payload) {
  state.drawerButtonMini = payload
}

export function updateMiniModeForced (state, payload) {
  state.miniModeForced = payload
}

export function updateShowDrawerButton (state, payload) {
  state.showDrawerButton = payload
}

export function updateActivePath (state, payload) {
  state.activePath = payload
}
/* *
 * Only for the first time you want to set a menu
 * */
export const menuSet = (state, payload) => {
  // console.log((state.user && !state.user.id), state, !state.user.id)
  if (state.menu) {
    state.menu = payload
  } else {
    console.error('You can\'t use set menu mutation, if is already a menu set! Please check your code and use update for this mutation of menu!')
  }
}

/* *
 * Any time you want to update a menu
 * */
export const menuUpdate = (state, payload) => {
  state.menu = payload
}
