export function updateDrawerState ({ state, commit }, payload = null) {
  if (payload === null) {
    commit('updateDrawerState', !state.leftDrawerOpen)
  } else {
    commit('updateDrawerState', payload)
  }
}

export function updateActivePath ({ state, commit }, payload = null) {
  console.log(payload)

  if (payload && payload !== null) {
    commit('updateActivePath', payload)
  } else {
    commit('updateActivePath', '')
  }
}

export function updateDrawerButtonMini ({ state, commit, dispatch }, payload = null) {
  if (payload !== true && payload !== false) {
    if (state.drawerButtonMini === true) {
      commit('updateMiniModeForced', false)
    } else {
      commit('updateMiniModeForced', true)
    }
  }
  if (payload === null) {
    commit('updateDrawerButtonMini', !state.drawerButtonMini)
  } else {
    commit('updateDrawerButtonMini', payload)
  }
}

export const menuSet = ({ dispatch, commit, state }, payload) => {
  if (state.menu) {
    commit('menuSet', payload)
  } else {
    console.error('You can\'t use set menu action, if is already a menu set! Please check your code and use update for this action!')
  }
}

export const menuUpdate = ({ dispatch, commit }, payload) => {
  payload = payload || {}

  if (payload) {
    commit('menuUpdate', payload)
  } else {
    console.error('You can\'t use "update" user action, if you want to unset user! Please check your code and use "userUnset" for this action!')
  }
}

export function updateShowDrawerButton ({ state, commit }, payload = null) {
  if (payload === null) {
    commit('updateShowDrawerButton', !state.showDrawerButton)
  } else {
    commit('updateShowDrawerButton', payload)
  }
}
