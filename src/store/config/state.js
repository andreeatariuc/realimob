export default {
  menu: {},
  leftDrawerOpen: true,
  showDrawerButton: true,
  drawerButtonMini: false,
  miniModeForced: false,
  activePath: ''
}
