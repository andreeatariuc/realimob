export function emailIsValid (value) {
  if (value.length > 5) {
    return /\S+@\S+\.\S+/.test(value)
  }
}

export function checkIfIsValidRomanianPhoneNumber (value) {
  let dev = 1
  if (dev === 1) {
    console.log(value)
  }
  if (value.length >= 10) {
    if (/^\d+$/.test(value)) {
      if (countApperanceOfString(value, '07') >= 1) {
        return true
      } else {
        return false
      }
    } else {
      return false
    }
  } else {
    return false
  }
}

export function countApperanceOfString (value, needle) {
  value += ''
  needle += ''

  if (value.length <= 0) {
    return 0
  }

  let subStr = needle.replace(/[.*+?^${}()|[\]\\]/g, '\\$&')
  return (value.match(new RegExp(subStr, 'gi')) || []).length
}

export function passwordChecker (value) {
  if (value.length === 0) {
    return true
  }
  let allowedSpecial = /[!@#$%^&*()_+\-=[\]{};':"\\|,.<>/?]+/
  let lower = false
  let upper = false
  let numbers = false
  let special = false
  if (allowedSpecial.test(value)) {
    special = true
  }
  if (value.length >= 5) {
    if (/^\d+$/.test(value)) {
      numbers = true
    }
    for (let i = 0; i < value.length; i++) {
      if (isNaN(value[i] * 1)) {
        if (/[A-Z]|[\u0080-\u024F]/.test(value[i]) && value[i] === value[i].toUpperCase()) {
          upper = true
        }

        if (/[a-z]|[\u0080-\u024F]/.test(value[i]) && value[i] === value[i].toLowerCase()) {
          lower = true
        }
      } else {
        numbers = true
      }
    }

    if (numbers && upper && lower && special) {
      return true
    } else {
      return false
    }
  } else {
    return false
  }
}
