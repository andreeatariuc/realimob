/*
 * This file (which will be your service worker)
 * is picked up by the build system ONLY if
 * quasar.conf > pwa > workboxPluginMode is set to "InjectManifest"
 */
// console.log(navigator.serviceWorker)

if (navigator.serviceWorker) {
  navigator.serviceWorker.getRegistrations()
    .then(
      function (registrations) {
        for (let registration of registrations) {
          registration.unregister()
        }
      }
    )
}
